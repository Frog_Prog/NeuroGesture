package com.yaz.korzh.controller;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.yaz.korzh.controller.MainActivity.gesttime;


public class MainActivity extends AppCompatActivity implements SensorEventListener {

    public static final int gesttime = 2000;
    private SensorManager msensorManager; //Менеджер сенсоров аппрата
    public static String vsplx = "vp";
    private float[] rotationMatrix;     //Матрица поворота
    private float[] accelData;           //Данные с акселерометра
    private float[] magnetData;       //Данные геомагнитного датчика
    private float[] OrientationData; //Матрица положения в пространстве
    float[] valuesAccel = new float[3];
    float[] valuesAccelMotion = new float[3];
    float[] valuesAccelGravity = new float[3];
    float[] valuesLinAccel = new float[3];
    float[] valuesGravity = new float[3];
    Sensor sensorAccel;
    Sensor sensorLinAccel;
    Sensor sensorGravity;
    boolean isReg = false;
    ArrayList linX = new ArrayList();
    ArrayList linY = new ArrayList();
    ArrayList linZ = new ArrayList();
    ArrayList<Long> times = new ArrayList<Long>();
    ArrayList<Point> points = new ArrayList<Point>();
    long startSec;
    public EditText ET;
    public String namegest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        msensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorAccel = msensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorLinAccel = msensorManager
                .getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorGravity = msensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        rotationMatrix = new float[16];
        accelData = new float[3];
        magnetData = new float[3];
        OrientationData = new float[3];
        String[] data = {"Norm", "LineRight", "Round"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) findViewById(R.id.editText);
        spinner.setAdapter(adapter);
        // заголовок
        spinner.setPrompt("Title");
        // выделяем элемент
        spinner.setSelection(2);
        // устанавливаем обработчик нажатия
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // показываем позиция нажатого элемента
                //Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
                namegest = parent.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
       // ET = findViewById(R.id.editText);
//        Point[] pes = new Point[]{
//                new Point(2, 2, 2, 0),
//                new Point(1, 1, 1, 150),
//                new Point(1, 1, 1, 250),
//                new Point(1, 1, 1, 350),
//                new Point(1, 1, 1, 400),
//                new Point(1, 1, 1, 500),
//                new Point(1, 1, 1, 650),
//                new Point(1, 1, 1, 700),
//                new Point(1, 1, 1, 850),
//                new Point(2, 2, 2, 950),
//                new Point(3, 3, 3, 1000)
//        };
//
//        Gesture gest = new Gesture("none", pes);
    }

    @Override
    protected void onResume() {
        super.onResume();
        msensorManager.registerListener(this, msensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
        msensorManager.registerListener(this, msensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_UI);
        msensorManager.registerListener(this, msensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
        msensorManager.registerListener(this, sensorAccel,
                SensorManager.SENSOR_DELAY_NORMAL);
        msensorManager.registerListener(this, sensorLinAccel,
                SensorManager.SENSOR_DELAY_NORMAL);
        msensorManager.registerListener(this, sensorGravity,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                for (int i = 0; i < 3; i++) {
                    valuesAccel[i] = sensorEvent.values[i];
                    valuesAccelGravity[i] = (float) (0.1 * sensorEvent.values[i] + 0.9 * valuesAccelGravity[i]);
                    valuesAccelMotion[i] = sensorEvent.values[i]
                            - valuesAccelGravity[i];
                }
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                for (int i = 0; i < 3; i++) {
                    valuesLinAccel[i] = sensorEvent.values[i];
                }
                if (isReg) {

                    //  linX.add(valuesLinAccel[0]);
                    //  linY.add(valuesLinAccel[1]);
                    //  linZ.add(valuesLinAccel[2]);
                    //  times.add(System.currentTimeMillis());
                    points.add(new Point(valuesLinAccel[0], valuesLinAccel[1], valuesLinAccel[2], (System.nanoTime() / 1000000) - startSec));
                    //Lyog.d("lincol",linX.toString());

                }
                break;
            case Sensor.TYPE_GRAVITY:
                for (int i = 0; i < 3; i++) {
                    valuesGravity[i] = sensorEvent.values[i];
                }
                break;
        }
        loadNewSensorData(sensorEvent); // Получаем данные с датчика
        SensorManager.getRotationMatrix(rotationMatrix, null, accelData, magnetData); //Получаем матрицу поворота
        SensorManager.getOrientation(rotationMatrix, OrientationData); //Получаем данные ориентации устройства в пространстве


    }

    private void loadNewSensorData(SensorEvent event) {
        final int type = event.sensor.getType(); //Определяем тип датчика
        if (type == Sensor.TYPE_ACCELEROMETER) { //Если акселерометр
            accelData = event.values.clone();
        }

        if (type == Sensor.TYPE_MAGNETIC_FIELD) { //Если геомагнитный датчик
            magnetData = event.values.clone();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void Starting(View view) {

            isReg = true;
            startSec = System.nanoTime() / 1000000;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Save(new View(getApplicationContext()));
            }
        }, gesttime);
    }

    public void Stopping(View view) {
        isReg = false;
    }

    public void Save(View view) {
        Stopping(new View(this));

        Gesture gest = new Gesture(namegest, points.toArray(new Point[points.size()]));


        String[] str = new String[]{"http://192.168.1.22/test/index.php"};

        sendHttpGet(str[0], gest);

        Toast toast = Toast.makeText(getApplicationContext(),
                "Сохранено", Toast.LENGTH_SHORT);
        toast.show();


    }

    private void sendHttpGet(String url, final Gesture gest) {

        class HttpGetAsyncTask extends AsyncTask<String, Void, String> {

            private String lmresult = null;


            String bowlingJson() throws JSONException, IOException {

                JSONArray ar = new JSONArray();
                JSONObject obj = new JSONObject();


                ar.put(0, gest);
                points = new ArrayList<Point>();


                obj.put(gest.m_name, gest);

                return gest.toJson();
            }



            @Override
            protected String doInBackground(String... params) {

                String url = params[0];

                OkHttpClient client = new OkHttpClient();
//
                String json = null;
                try {
                    try {
                        json = "gesture=" + bowlingJson();

                        json += "&action=insert_gesture";
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                RequestBody body = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=utf-8"), json);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();

                try {
                    Response response = client.newCall(request).execute();

                    vsplx = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                    vsplx = "vseo ploxaaa";
                }

                return null;
            }

            // Argument comes for this method according to the return type of the doInBackground() and
            //it is the third generic type of the AsyncTask
            @Override
            protected void onPostExecute(String result) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Отправлено", Toast.LENGTH_SHORT);
                toast.show();
                super.onPostExecute(result);


            }
        }

        // Initialize the AsyncTask class
        HttpGetAsyncTask httpGetAsyncTask = new HttpGetAsyncTask();
        httpGetAsyncTask.execute(url);

    }


}

class Point {
    float lX;
    float lY;
    float lZ;
    long sec;

    public Point(float X, float Y, float Z, long T) {
        lX = X;
        lY = Y;
        lZ = Z;
        sec = T;
    }


}

class Gesture {

    int m_gesttime;

    ArrayList<Long> grid = new ArrayList<>();
    final long TIME = 5;
    int TTT = 0;
    String m_name;
    Point[] m_points;
    ArrayList<Point> ps = new ArrayList<>();

    public Gesture(String name, Point[] points) {
        m_gesttime = gesttime;


        m_name = name;
        m_points = points;
//        Point first = new Point(100,200,300,0);
//        Point second = new Point(200,300,400,50);
        fixTime();
        Grid();
        ArrayList<Long> longes = grid;
        linInterArr();
//        Log.e("left", String.valueOf(findLeft(900).sec));
//        Log.e("right", String.valueOf(findRight(50).sec));
//        Log.e("calc", String.valueOf(calculateLinear(0.0f, 200.0f, 100.0f, 150.0f, 100.0f)));
    }

    private void fixTime() {
        long zerotime = m_points[0].sec;
        for (int i = 0; i < m_points.length; i++) {
            m_points[i].sec -= zerotime;
        }
    }

    private Point findLeft(long time) {
        Point left = null;
        for (int i = 0; i < m_points.length - 1; i++) {
            if (m_points[i].sec > time) {
                break;
            } else {
                left = m_points[i];
            }
        }
        return left;
    }

    private Point findRight(long time) {
        Point right = null;
        for (int i = m_points.length - 1; i > -1; i--) {
            if (m_points[i].sec < time) {
                break;
            } else {
                right = m_points[i];
            }


        }
        return right;
    }

    private void Grid() {
        ArrayList<Long> grid = new ArrayList<>();
        for (int i = 0; i <= gesttime; i += TIME) {
            grid.add((long) i);
        }
        this.grid = grid;
    }

    private ArrayList<Point> linInterArr() {
        ArrayList<Point> true_points = new ArrayList<>();
        for (int i = 0; i < grid.size() - 1; i++) {
            if (findLeft(grid.get(i)) != null && findRight(grid.get(i)) != null) {
                Point toAdd = linInter(findLeft(grid.get(i)), findRight(grid.get(i)), TIME);

                ps.add(toAdd);

            } else {
                ps.add(new Point(0, 0, 0, grid.get(i)));
            }


        }


        ps.remove(0);
        return true_points;
    }

    private Point linInter(Point one, Point two, long delta) {
        //Point[] toes = new Point[(int) (((two.sec - one.sec) / delta))];
        //for (int i = 0; i < toes.length; i++) {
        float x = calculateLinear(one.sec, two.sec, TTT, one.lX, two.lX);
        float y = calculateLinear(one.sec, two.sec, TTT, one.lY, two.lY);
        float z = calculateLinear(one.sec, two.sec, TTT, one.lZ, two.lZ);
        Point ret = new Point(x, y, z, TTT);
        TTT += TIME;
        //}
        return ret;
    }

    private float calculateLinear(float t1, float t2, float t, float y1, float y2) {
        float y = y1 + (t - t1) * ((y2 - y1) / (t2 - t1));
        if(t2==t1){
            y=(y1+y2)/2;
        }
        return y;
    }

    public String toJson(){
        String s = "{ \"Gesture\":{ \"name\":" + "\"" + m_name + "\"" + ", \"TIME\":" + TIME + "," + "\"gesttime\":" + m_gesttime + ",\"points\":[";
        ArrayList<String> points = new ArrayList<>();
        for (int i = 0; i < ps.size(); i++) {
            Point p = ps.get(i);
            String so = "{" + "\"x\":" + p.lX + ",\"y\":" + p.lY + ",\"z\":" + p.lZ + ",\"t\":" + p.sec + "}";
            points.add(so);

        }
        String sp = "";
        for (int i = 0; i < points.size(); i++) {
            sp += points.get(i);
            if (i != points.size() - 1) {
                sp += ",";
            }
        }
        s += sp;
        s += "]}}";
       // s.replace('\"',' ');
        Log.d("json", s);
        return s;

    }

    @Override
    public String toString() {

        String s = "{ \"Gesture\":{ \"name\":" + "\"" + m_name + "\"" + ", \"TIME\":" + TIME + "," + "\"gesttime\":" + m_gesttime + ",\"points\":[";
        ArrayList<String> points = new ArrayList<>();
        for (int i = 0; i < ps.size(); i++) {
            Point p = ps.get(i);
            String so = "{" + "\"x\":" + p.lX + ",\"y\":" + p.lY + ",\"z\":" + p.lZ + ",\"t\":" + p.sec + "}";
            points.add(so);

        }
        String sp = "";
        for (int i = 0; i < points.size(); i++) {
            sp += points.get(i);
            if (i != points.size() - 1) {
                sp += ",";
            }
        }
        s += sp;
        s += "]}}";
        s.replace('\"',' ');
        Log.d("json", s);
        return s;

    }
}