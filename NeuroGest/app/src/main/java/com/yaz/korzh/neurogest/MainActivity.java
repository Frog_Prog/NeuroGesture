package com.yaz.korzh.neurogest;


import android.content.Context;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.VibrationEffect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.IOException;

import android.widget.Toast;

import java.util.ArrayList;

import java.util.Random;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import android.os.Vibrator;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private float[] rotationMatrix;     //Матрица поворота
    private float[] accelData;           //Данные с акселерометра
    private float[] magnetData;       //Данные геомагнитного датчика
    private float[] OrientationData; //Матрица положения в пространстве

    float[] valuesAccel = new float[3];
    float[] valuesAccelMotion = new float[3];
    float[] valuesAccelGravity = new float[3];
    float[] valuesLinAccel = new float[3];
    float[] valuesGravity = new float[3];
    Sensor sensorAccel;
    Sensor sensorLinAccel;
    Sensor sensorGravity;

    boolean isReg = false;


    ImageButton imageButton;
    public static final int gesttime = 2000;
    private SensorManager msensorManager; //Менеджер сенсоров аппрата
    ArrayList<Long> times = new ArrayList<Long>();
    ArrayList<Point> points = new ArrayList<Point>();
    long startSec;
    public TextView txt;
    String[] data = { "LineRight", "Round"};;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);



        msensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorAccel = msensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorLinAccel = msensorManager
                .getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorGravity = msensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        rotationMatrix = new float[16];
        accelData = new float[3];
        magnetData = new float[3];
        OrientationData = new float[3];


        imageButton =(ImageButton)findViewById(R.id.imageButton5);
        Float[][] test_data = new Float[399][3];
        for(int g =0; g<test_data.length;g++){
            Random rand = new Random();
            for(int i=0; i<3;i++){
                test_data[g][i]= rand.nextFloat();
            }
        }
        float[] an_data = new float[1197];
        for(int g =0; g<an_data.length;g++){
            Random rand = new Random();

            an_data[g]= rand.nextFloat();

        }
        createAndUseNetwork(an_data);

    }

    private float[] createAndUseNetwork(float[] an_data) {

        int[] out = new int[3];


        float[] result = new float[2];

        TensorFlowInferenceInterface inferenceInterface = new TensorFlowInferenceInterface(getAssets(), "model.h5.pb");
        String INPUT_NODE = "conv1d_7_input"; // input tensor name
        String OUTPUT_NODE = "output_node0"; // output tensor name
        String[] OUTPUT_NODES = {"output_node0"};
        float[] fff = new float[]{1,2,3};
        //Log.d("topot", "test1");
        inferenceInterface.feed(INPUT_NODE, an_data, 1, 399, 3);
        //Log.d("topot", "test2");
        inferenceInterface.run(OUTPUT_NODES);
        //Log.d("topot", "test3");
        inferenceInterface.fetch(OUTPUT_NODE, result);
        //Log.d("topot", "test4");
        Log.d("topot", String.valueOf(result[0])+" " +String.valueOf(result[1]));
        Toast toast = Toast.makeText(getApplicationContext(),
                "Распознано", Toast.LENGTH_SHORT);
        //toast.show();
        return result;

    }

    @Override
    protected void onResume() {
        super.onResume();
        msensorManager.registerListener(this, msensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
        msensorManager.registerListener(this, msensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_UI);
        msensorManager.registerListener(this, msensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
        msensorManager.registerListener(this, sensorAccel,
                SensorManager.SENSOR_DELAY_NORMAL);
        msensorManager.registerListener(this, sensorLinAccel,
                SensorManager.SENSOR_DELAY_NORMAL);
        msensorManager.registerListener(this, sensorGravity,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                for (int i = 0; i < 3; i++) {
                    valuesAccel[i] = sensorEvent.values[i];
                    valuesAccelGravity[i] = (float) (0.1 * sensorEvent.values[i] + 0.9 * valuesAccelGravity[i]);
                    valuesAccelMotion[i] = sensorEvent.values[i]
                            - valuesAccelGravity[i];
                }
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                for (int i = 0; i < 3; i++) {
                    valuesLinAccel[i] = sensorEvent.values[i];
                }
                if (isReg) {

                    //  linX.add(valuesLinAccel[0]);
                    //  linY.add(valuesLinAccel[1]);
                    //  linZ.add(valuesLinAccel[2]);
                    //  times.add(System.currentTimeMillis());
                    points.add(new Point(valuesLinAccel[0], valuesLinAccel[1], valuesLinAccel[2], (System.nanoTime() / 1000000) - startSec));
                    //Lyog.d("lincol",linX.toString());

                }
                break;
            case Sensor.TYPE_GRAVITY:
                for (int i = 0; i < 3; i++) {
                    valuesGravity[i] = sensorEvent.values[i];
                }
                break;
        }
        loadNewSensorData(sensorEvent); // Получаем данные с датчика
        SensorManager.getRotationMatrix(rotationMatrix, null, accelData, magnetData); //Получаем матрицу поворота
        SensorManager.getOrientation(rotationMatrix, OrientationData); //Получаем данные ориентации устройства в пространстве


    }

    private void loadNewSensorData(SensorEvent event) {
        final int type = event.sensor.getType(); //Определяем тип датчика
        if (type == Sensor.TYPE_ACCELEROMETER) { //Если акселерометр
            accelData = event.values.clone();
        }

        if (type == Sensor.TYPE_MAGNETIC_FIELD) { //Если геомагнитный датчик
            magnetData = event.values.clone();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private void sendHttpGet(String url) {

        class HttpGetAsyncTask extends AsyncTask<String, Void, String> {

            private String lmresult = null;






            @Override
            protected String doInBackground(String... params) {

                String url = params[0];

                OkHttpClient client = new OkHttpClient();
//
                String json = null;


                RequestBody body = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=utf-8"), "sidariussidurius");

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();

                try {
                    Response response = client.newCall(request).execute();
                    Log.e("response", response.message());


                } catch (IOException e) {
                    e.printStackTrace();

                }

                return null;
            }

            // Argument comes for this method according to the return type of the doInBackground() and
            //it is the third generic type of the AsyncTask
            @Override
            protected void onPostExecute(String result) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Отправлено", Toast.LENGTH_SHORT);
               // toast.show();
                super.onPostExecute(result);


            }
        }

        // Initialize the AsyncTask class
        HttpGetAsyncTask httpGetAsyncTask = new HttpGetAsyncTask();
        httpGetAsyncTask.execute(url);

    }


    public void Starting(View view) {

        isReg = true;
        startSec = System.nanoTime() / 1000000;

        imageButton.setImageResource(R.drawable.active);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                try {
                    Save(new View(getApplicationContext()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, gesttime);
    }

    public void Stopping(View view) {
        isReg = false;
    }

    public void Save(View view) throws IOException {
        Stopping(new View(this));

        Gesture gest = new Gesture("record", points.toArray(new Point[points.size()]));

        float[] data_gest= new float[1197];
        int g =0;
        for(int i = 0; i<gest.m_points.length; i++){

                Point p = gest.m_points[i];
                data_gest[g]=p.lX;
                g++;
                data_gest[g]=p.lY;
                g++;
                data_gest[g]=p.lZ;
                g++;
        }

       // data_gest=new float[]{-0.16945705f,0.632647f,-0.76444024f,-0.16153197f,0.628682f,-0.75386703f,-0.1536069f,0.624717f,-0.7432938f,-0.14568183f,0.6207520400000001f,-0.7327205999999999f,-0.13775676f,0.6167870999999999f,-0.7221474f,-0.1298317f,0.6128220999999999f,-0.7115741999999999f,-0.12190662000000001f,0.60885715f,-0.701001f,-0.11398156f,0.60489213f,-0.6904277999999999f,-0.10605649f,0.6009272f,-0.6798546f,-0.09813142f,0.5969622f,-0.66928136f,-0.090206355f,0.59299725f,-0.65870816f,-0.08228128400000001f,0.5890323f,-0.64813495f,-0.07435621f,0.5850673f,-0.63756174f,-0.06643114f,0.5811023f,-0.62698853f,-0.05850608f,0.57713735f,-0.6164153f,-0.05058101f,0.5731724f,-0.6058420999999999f,-0.04265593f,0.56920743f,-0.5952689f,-0.034730867000000006f,0.5652423999999999f,-0.5846956999999999f,-0.026805803000000003f,0.5612774500000001f,-0.5741225f,-0.018880725f,0.5573125f,-0.5635492999999999f,-0.010955662000000001f,0.5533475f,-0.5529761f,-0.0030305982000000003f,0.54938257f,-0.54240286f,0.0048944800000000005f,0.54541755f,-0.53182966f,0.0128195435f,0.5414526f,-0.52125645f,0.020744622f,0.5374876f,-0.51068324f,0.028669685f,0.53352267f,-0.50011003f,0.03659475f,0.5295577f,-0.48953682f,0.044519827000000005f,0.5255927f,-0.4789636f,0.05244489f,0.5216277f,-0.4683904f,0.060369954f,0.51766276f,-0.4578172f,0.06829503f,0.5136978f,-0.447244f,0.076220095f,0.50973284f,-0.43667078000000004f,0.08414516f,0.5057678f,-0.42609757000000004f,0.09207025f,0.50180286f,-0.41552436f,0.099995315f,0.4978379f,-0.40495116000000003f,0.10792038f,0.49387294000000004f,-0.39437795000000003f,0.11584544000000001f,0.48990795000000004f,-0.38380474000000003f,0.123770505f,0.48594296000000003f,-0.37323153000000003f,0.13169557f,0.48197799999999996f,-0.36265832000000003f,0.13962066f,0.47801304f,-0.3520851f,0.14658551f,0.48309749999999996f,-0.36605778f,0.15355034f,0.48818192f,-0.38003045f,0.16051519f,0.49326637f,-0.39400315f,0.16748002f,0.49835083f,-0.40797582f,0.17444487f,0.50343525f,-0.4219485f,0.18140972f,0.5085197f,-0.43592119999999995f,0.18837455f,0.51360416f,-0.44989386000000003f,0.1953394f,0.5186885999999999f,-0.46386653f,0.20230424f,0.5237731f,-0.47783919999999996f,0.20926908f,0.52885747f,-0.49181187f,0.21623392f,0.5339419f,-0.5057846f,0.22319877000000002f,0.5390264f,-0.5197573f,0.2301636f,0.54411083f,-0.5337299f,0.23712844f,0.5491952999999999f,-0.5477025999999999f,0.24409329999999999f,0.5542796999999999f,-0.56167525f,0.25105813f,0.55936414f,-0.57564795f,0.25802296f,0.5644486f,-0.5896205999999999f,0.26498783f,0.56953305f,-0.6035933f,0.27195266f,0.5746175f,-0.617566f,0.2789175f,0.57970196f,-0.6315386f,0.28588235f,0.5847863999999999f,-0.6455113f,0.2928472f,0.5898708f,-0.659484f,0.29981202f,0.59495527f,-0.67345667f,0.30677688000000003f,0.6000397f,-0.68742937f,0.31374168f,0.6051242f,-0.70140207f,0.32070655f,0.61020863f,-0.7153746999999999f,0.3276714f,0.615293f,-0.7293473500000001f,0.3346362f,0.6203775f,-0.7433200999999999f,0.34160107f,0.62546194f,-0.7572927500000001f,0.34856594f,0.6305464f,-0.7712654f,0.35553074f,0.63563085f,-0.7852380999999999f,0.3624956f,0.64071524f,-0.7992108f,0.36946043f,0.6457997600000001f,-0.8131834f,0.37642527000000003f,0.65088415f,-0.8271561f,0.38339013f,0.6559686f,-0.8411288f,0.39035496000000003f,0.66105306f,-0.85510147f,0.3973198f,0.6661374999999999f,-0.86907417f,0.40428466f,0.671222f,-0.8830467999999999f,0.4112495f,0.67630637f,-0.8970195f,0.42035455f,0.6771279f,-0.8977828999999999f,0.4326699f,0.67155504f,-0.8787324999999999f,0.44498527f,0.6659822f,-0.859682f,0.45730063f,0.66040933f,-0.8406315400000001f,0.469616f,0.6548364999999999f,-0.82158107f,0.48193139999999995f,0.6492635999999999f,-0.8025306f,0.49424672000000003f,0.64369076f,-0.7834801f,0.5065621f,0.6381179f,-0.7644297f,0.51887745f,0.632545f,-0.7453792f,0.53119284f,0.62697214f,-0.72632873f,0.5435082f,0.6213993f,-0.70727825f,0.55582356f,0.6158264f,-0.6882278f,0.56813896f,0.6102536f,-0.6691773f,0.5804543f,0.6046807f,-0.6501268f,0.5927696f,0.59910786f,-0.63107634f,0.605085f,0.5935349999999999f,-0.61202586f,0.6174004f,0.58796215f,-0.5929753999999999f,0.62971574f,0.5823893f,-0.5739249f,0.64203113f,0.57681644f,-0.5548744999999999f,0.65434647f,0.5712435f,-0.535824f,0.66666186f,0.56567067f,-0.5167735f,0.67897725f,0.5600978f,-0.49772304f,0.6912925999999999f,0.5545249600000001f,-0.4786726f,0.7036079f,0.5489520999999999f,-0.4596221f,0.7159232999999999f,0.54337925f,-0.44057164f,0.7282387f,0.5378064f,-0.4215212f,0.74055403f,0.53223354f,-0.40247069999999996f,0.75286937f,0.5266607f,-0.38342023000000003f,0.76518476f,0.5210878f,-0.36436975f,0.7775001500000001f,0.51551497f,-0.34531927f,0.7898155f,0.50994205f,-0.32626879999999997f,0.8021307999999999f,0.50436926f,-0.30721837f,0.8144462f,0.49879634f,-0.2881679f,0.8267616f,0.4932235f,-0.26911741f,0.839077f,0.48765063000000003f,-0.25006694f,0.8513923f,0.48207778f,-0.23101646f,0.86370766f,0.47650492f,-0.21196598f,0.87602305f,0.47093207000000004f,-0.1929155f,0.88833845f,0.4653592f,-0.17386508f,0.9006538f,0.45978636f,-0.1548146f,0.8965365f,0.4512272f,-0.14089875f,0.88146424f,0.44067714f,-0.13040602f,0.8663919600000001f,0.4301271f,-0.11991329f,0.8513197f,0.4195771f,-0.10942056f,0.8362474f,0.40902704f,-0.09892782600000001f,0.8211750999999999f,0.39847702f,-0.0884351f,0.8061028f,0.38792699999999997f,-0.07794237f,0.7910305f,0.37737694f,-0.067449644f,0.77595824f,0.36682692f,-0.05695691f,0.76088595f,0.35627687f,-0.046464182f,0.74581367f,0.34572685000000003f,-0.035971455f,0.7307414f,0.33517683f,-0.02547872f,0.7156691f,0.32462677f,-0.014985993500000001f,0.7005968f,0.31407672000000003f,-0.0044932663f,0.6855245f,0.3035267f,0.005999461f,0.67045224f,0.29297668f,0.016492188f,0.65537995f,0.28242666f,0.02698493f,0.6403076600000001f,0.27187659999999997f,0.037477657000000004f,0.6252354f,0.26132655f,0.047970384000000005f,0.6101631f,0.25077653f,0.05846311f,0.5950907999999999f,0.24022649999999998f,0.06895584f,0.5800185f,0.22967647f,0.079448566f,0.56494623f,0.21912643f,0.08994131f,0.5498739500000001f,0.20857641000000002f,0.100434035f,0.53480166f,0.19802636f,0.11092675f,0.5197294f,0.18747634f,0.12141949f,0.5046571f,0.17692631f,0.13191223f,0.4895848f,0.16637626f,0.14240494f,0.47451252f,0.15582624f,0.15289769f,0.45944023f,0.14527619f,0.1633904f,0.44436795f,0.13472617f,0.17388314f,0.42929566f,0.124176145f,0.18437585f,0.41422337000000004f,0.11362609f,0.1948686f,0.3991511f,0.10307607f,0.20536134f,0.3840788f,0.09252602f,0.21585405f,0.36900649999999996f,0.081976f,0.22634679f,0.35393423f,0.071425974f,0.23683949999999998f,0.33886194000000003f,0.060875922000000006f,0.24733225f,0.32378966f,0.0503259f,0.25782499999999997f,0.30827296f,0.041362792f,0.2692424f,0.2909787f,0.038747497000000006f,0.28435877f,0.2736844f,0.036132199999999996f,0.29947513000000003f,0.25639015f,0.033516902f,0.31459147f,0.23909588f,0.030901605000000002f,0.32970783f,0.22180161f,0.028286308000000003f,0.34482419999999997f,0.20450735f,0.02567101f,0.35994056f,0.18721306000000001f,0.023055714f,0.37505692f,0.16991879999999998f,0.020440418000000002f,0.39017326f,0.15262453f,0.017825121000000003f,0.40528962f,0.13533026f,0.015209824f,0.42040598f,0.11803599000000001f,0.012594527000000001f,0.43552235f,0.10074171400000001f,0.009979229000000001f,0.4506387f,0.08344744f,0.007363934000000001f,0.46575505f,0.06615318f,0.004748635f,0.4808714f,0.04885891f,0.0021333396f,0.49598777f,0.031564623f,-0.00048195943f,0.5111041f,0.014270365f,-0.0030972548f,0.52622044f,-0.0030238926f,-0.005712554000000001f,0.54133683f,-0.02031818f,-0.008327849f,0.5564532f,-0.037612440000000004f,-0.010943145000000001f,0.57156956f,-0.054906726f,-0.013558444000000001f,0.5866859f,-0.07220098400000001f,-0.016173739f,0.60180223f,-0.08949524f,-0.018789038f,0.6169186f,-0.10678953000000001f,-0.021404333f,0.632035f,-0.12408379f,-0.024019629f,0.64715135f,-0.14137807f,-0.026634932f,0.6622677f,-0.15867233f,-0.029250227f,0.677384f,-0.17596659f,-0.031865522f,0.6925004f,-0.19326085f,-0.034480818f,0.7076167999999999f,-0.21055514f,-0.037096113f,0.72273314f,-0.22784942f,-0.039711416f,0.7378494999999999f,-0.24514371000000001f,-0.042326710000000003f,0.7529658f,-0.26243794f,-0.044942006f,0.7680821999999999f,-0.27973223f,-0.0475573f,0.78319854f,-0.29702652f,-0.050172597000000006f,0.7983148999999999f,-0.31432074f,-0.0527879f,0.81343126f,-0.33161503000000003f,
        //        -0.055403195f,0.82854766f,-0.34890932f,-0.05801849f,0.843664f,-0.36620359999999996f,-0.060633786f,0.8587802999999999f,-0.38073385f,-0.062025808f,0.8663677f,-0.38420814000000003f,-0.05852474f,0.8438389f,-0.38768244f,-0.055023666000000006f,0.82131004f,-0.39115673f,-0.051522598f,0.7987812999999999f,-0.39463103f,-0.04802153f,0.77625245f,-0.39810535f,-0.04452046f,0.7537235999999999f,-0.40157965f,-0.041019388000000004f,0.7311947999999999f,-0.40505394f,-0.037518322f,0.70866597f,-0.40852824f,-0.03401725f,0.6861372f,-0.41200256f,-0.030516181000000003f,0.6636084f,-0.41547686f,-0.027015112f,0.64107955f,-0.41895115f,-0.023514044f,0.6185507f,-0.42242545000000004f,-0.020012975000000002f,0.5960219f,-0.42589974f,-0.016511906f,0.5734931f,-0.42937404f,-0.013010833000000001f,0.5509643f,-0.43284836f,-0.009509765f,0.52843547f,-0.43632266000000003f,-0.006008696f,0.5059066999999999f,-0.43979695f,-0.002507627f,0.48337784f,-0.44327125f,0.0009934455f,0.46084905f,-0.44674557000000004f,0.0044945106f,0.43832022f,-0.45021987f,0.007995583f,0.4157914f,-0.45369416f,0.011496648f,0.39326259999999996f,-0.45716846f,0.014997721f,0.3707338f,-0.46064276f,0.018498793f,0.34820497f,-0.46411705000000003f,0.021999858f,0.32567614f,-0.46759138f,0.02550093f,0.30314732f,-0.47106567f,0.029001996000000002f,0.28061855f,-0.47453997000000003f,0.03250307f,0.25808972f,-0.47801426f,0.036004140000000004f,0.2355609f,-0.4814886f,0.039505206f,0.21303207000000002f,-0.48496288f,0.04300628f,0.19050324f,-0.48843718f,0.046507344000000006f,0.16797447000000001f,-0.49191147f,0.050008416f,0.14544564000000001f,-0.49538577f,0.05350948f,0.12291682f,-0.49886006f,0.057010554000000005f,0.10038799f,-0.50233436f,0.060511626000000006f,0.07785916f,-0.5058087f,0.06401269f,0.055330396000000004f,-0.50928295f,0.067513764f,0.03280157f,-0.5127573f,0.07101484f,0.010272741f,-0.5162316f,0.074515894f,-0.012256086000000001f,-0.51888484f,0.07797398f,-0.02811025f,-0.51825404f,0.08126005f,-0.017265711f,-0.51762325f,0.08454612f,-0.0064211693000000005f,-0.51699245f,0.08783218f,0.0044233724f,-0.51636165f,0.09111825400000001f,0.015267912000000002f,-0.51573086f,0.09440432500000001f,0.026112456000000003f,-0.51510006f,0.09769039600000001f,0.036956996f,-0.51446927f,0.10097647f,0.047801540000000003f,-0.51383847f,0.10426254f,0.058646075000000006f,-0.5132076999999999f,0.10754861f,0.06949062f,-0.5125769f,0.11083468f,0.08033516f,-0.5119461f,0.11412075f,0.0911797f,-0.5113153f,0.11740681500000001f,0.10202424f,-0.5106845f,0.12069289400000001f,0.11286878600000001f,-0.5100536999999999f,0.12397896f,0.12371333f,-0.5094229f,0.12726504f,0.13455787f,-0.5087921f,0.1305511f,0.1454024f,-0.5081612999999999f,0.13383716f,0.15624695f,-0.5075305f,0.13712324f,0.16709149f,-0.5068997f,0.14040932f,0.17793603f,-0.5062689f,0.14369538f,0.18878058f,-0.5056381f,0.14698145f,0.19962512000000002f,-0.5050072999999999f,0.15026753f,0.21046966f,-0.50437653f,0.15355359999999998f,0.22131419f,-0.50374573f,0.15683967000000001f,0.23215875f,-0.50311494f,0.16012573f,0.24300328000000002f,-0.50248414f,0.1634118f,0.2538478f,-0.50185335f,0.16669787f,0.26469237f,-0.50122255f,0.16998395f,0.2755369f,-0.50059175f,0.17327002f,0.28638145f,-0.49996093f,0.17655608f,0.29722598f,-0.49933013f,0.17984216f,0.30807054f,-0.49869934f,0.18312824f,0.31891507f,-0.49806854f,0.1864143f,0.3297596f,-0.49743775f,0.18970037f,0.34060416f,-0.49680695f,0.19298644f,0.35144868f,-0.49617615000000004f,0.1962725f,0.36229324f,-0.49554536f,0.19955859f,0.37313777000000004f,-0.49491453f,0.20284465000000002f,0.38398233f,-0.49428374f,0.20613073f,0.39482686f,-0.5121070999999999f,0.20846806f,0.36784992f,-0.54223317f,0.21017295f,0.31565863f,-0.57235926f,0.21187782f,0.26346734f,-0.6024854f,0.2135827f,0.21127607f,-0.6326115f,0.21528757f,0.1590848f,-0.6627375999999999f,0.21699244f,0.10689351f,-0.6928637f,0.21869731f,0.054702222f,-0.7229898f,0.2204022f,0.0025109649f,-0.7531159f,0.22210707000000002f,-0.049680322000000006f,-0.783242f,0.22381194000000001f,-0.10187161f,-0.8133680999999999f,0.22551681f,-0.15406287f,-0.8434942f,0.22722168f,-0.20625418f,-0.8736202999999999f,0.22892657f,-0.25844544f,-0.90374637f,0.23063144f,-0.31063676f,-0.93387246f,0.23233631000000002f,-0.36282802000000003f,-0.9639985600000001f,0.23404118000000002f,-0.41501927f,-0.99412465f,0.23574606f,-0.4672106f,-1.0242507f,0.23745093f,-0.51940185f,-1.0543768f,0.23915581f,-0.5715931f,-1.0845029f,0.24086069000000002f,-0.6237844f,-1.114629f,0.24256556f,-0.6759757f,-1.1447551f,0.24427043f,-0.72816694f,-1.1748812f,0.24597532f,-0.7803582f,-1.2050073000000001f,0.24768019f,-0.8325496f,-1.2351334f,0.24938506f,-0.8847408f,-1.2652595f,0.25108993f,-0.9369320999999999f,-1.2953856f,0.2527948f,-0.98912334f,-1.3255116999999998f,0.25449967f,-1.0413146f,-1.3556378f,0.25620455000000003f,-1.093506f,-1.3857639f,0.25790942f,-1.1456972f,-1.41589f,0.25961429999999996f,-1.1978885f,-1.4460161f,0.26131916f,-1.2500798f,-1.4761422f,0.26302406f,-1.302271f,-1.5062684f,0.26472893000000003f,-1.3544624f,-1.5363945f,0.2664338f,-1.4066536f,-1.5665206f,0.26813868f,-1.4588448999999999f,-1.5966467f,0.26984355f,-1.5110362f,-1.6267728f,0.27154842f,-1.5632274f,-1.6568988999999998f,0.2732533f,-1.6154187f,-1.6860465f,0.27648836f,-1.6400684f,-1.71128f,0.28584418f,-1.5545516f,-1.7365135f,0.29520002f,-1.4690347f,-1.7617471f,0.30455583f,-1.3835179f,-1.7869806f,0.31391165f,-1.298001f,-1.8122140999999998f,0.32326746f,-1.2124842f,-1.8374476f,0.3326233f,-1.1269674f,-1.8626812f,0.34197912f,-1.0414506f,-1.8879148f,0.35133493f,-0.95593375f,-1.9131483f,0.36069077f,-0.87041694f,-1.9383818f,0.3700466f,-0.7849001f,-1.9636152999999998f,0.3794024f,-0.6993832999999999f,-1.9888488999999998f,0.38875824000000003f,-0.6138664500000001f,-2.0140824f,0.39811406f,-0.5283496400000001f,-2.039316f,0.40746987f,-0.44283283f,-2.0645494f,0.41682569999999997f,-0.35731602f,-2.089783f,0.42618152000000004f,-0.27179919999999996f,-2.1150165f,0.43553734f,-0.1862824f,-2.14025f,0.44489318f,-0.100765586f,-2.1654835f,0.45424899999999996f,-0.0152487755f,-2.1907172f,0.4636048f,0.070268154f,-2.2159507f,0.47296065f,0.15578496f,-2.2411842f,0.4823165f,0.24130177f,-2.2664177f,0.49167228f,0.32681859999999996f,-2.2916512f,0.5010281f,0.41233539999999996f,-2.3168848f,0.51038396f,0.49785233f,-2.3421183f,0.5197397500000001f,0.583369f,-2.367352f,0.5290956f,0.6688859500000001f,-2.3925853f,0.5384514300000001f,0.75440264f,-2.417819f,0.5478071999999999f,0.8399195700000001f,-2.4430525f,0.55716306f,0.92543626f,-2.468286f,0.5665188999999999f,1.0109532f,-2.4935195f,0.5758747f,1.0964699f,-2.5187530000000002f,0.5852305f,1.1819868f,-2.5439866f,0.5945864f,1.2675037f,-2.56922f,0.60394216f,1.3530204f,-2.5944536f,0.613298f,1.4385374f,-2.619687f,0.62265384f,1.524054f,-2.6449208f,0.6320096f,1.6095709999999999f,-2.6701543f,0.64136547f,1.6950877f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f,0f};

        //String[] str = new String[]{"http://192.168.0.104/test/index.php"};

        //sendHttpGet(str[0], gest);

        float[] result=createAndUseNetwork(data_gest);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            //deprecated in API 26
            v.vibrate(500);
        }
        float max = 0;
        int gest_type=0;
        for(int i = 0; i<result.length; i++ ){
            if(max<result[i]){
                max=result[i];
                gest_type=i;
            }
        }
        //txt.setText(data[gest_type]);

            //sendHttpGet("http://192.168.1.150/apps/data/tima/toggle.lp");
        String urls = "http://192.168.1.150/apps/data/tima/toggle.lp?id="+String.valueOf(gest_type);

        sendHttpGet(urls);
        gest_type=0;


         points=new ArrayList<Point>();
        imageButton.setImageResource(R.drawable.passive);

    }



}

class Point {
    float lX;
    float lY;
    float lZ;
    long sec;

    public Point(float X, float Y, float Z, long T) {
        lX = X;
        lY = Y;
        lZ = Z;
        sec = T;
    }


}

class Gesture {

    int m_gesttime;

    ArrayList<Long> grid = new ArrayList<>();
    final long TIME = 5;
    int TTT = 0;
    String m_name;
    Point[] m_points;
    ArrayList<Point> ps = new ArrayList<>();

    public Gesture(String name, Point[] points) {
        m_gesttime = MainActivity.gesttime;


        m_name = name;
        m_points = points;
//        Point first = new Point(100,200,300,0);
//        Point second = new Point(200,300,400,50);
        fixTime();
        Grid();
        ArrayList<Long> longes = grid;
        linInterArr();
//        Log.e("left", String.valueOf(findLeft(900).sec));
//        Log.e("right", String.valueOf(findRight(50).sec));
//        Log.e("calc", String.valueOf(calculateLinear(0.0f, 200.0f, 100.0f, 150.0f, 100.0f)));
    }

    private void fixTime() {
        long zerotime = m_points[0].sec;
        for (int i = 0; i < m_points.length; i++) {
            m_points[i].sec -= zerotime;
        }
    }

    private Point findLeft(long time) {
        Point left = null;
        for (int i = 0; i < m_points.length - 1; i++) {
            if (m_points[i].sec > time) {
                break;
            } else {
                left = m_points[i];
            }
        }
        return left;
    }

    private Point findRight(long time) {
        Point right = null;
        for (int i = m_points.length - 1; i > -1; i--) {
            if (m_points[i].sec < time) {
                break;
            } else {
                right = m_points[i];
            }


        }
        return right;
    }

    private void Grid() {
        ArrayList<Long> grid = new ArrayList<>();
        for (int i = 0; i <= m_gesttime; i += TIME) {
            grid.add((long) i);
        }
        this.grid = grid;
    }

    private ArrayList<Point> linInterArr() {
        ArrayList<Point> true_points = new ArrayList<>();
        for (int i = 0; i < grid.size() - 1; i++) {
            if (findLeft(grid.get(i)) != null && findRight(grid.get(i)) != null) {
                Point toAdd = linInter(findLeft(grid.get(i)), findRight(grid.get(i)), TIME);

                ps.add(toAdd);

            } else {
                ps.add(new Point(0, 0, 0, grid.get(i)));
            }


        }


        ps.remove(0);
        return true_points;
    }

    private Point linInter(Point one, Point two, long delta) {
        //Point[] toes = new Point[(int) (((two.sec - one.sec) / delta))];
        //for (int i = 0; i < toes.length; i++) {
        float x = calculateLinear(one.sec, two.sec, TTT, one.lX, two.lX);
        float y = calculateLinear(one.sec, two.sec, TTT, one.lY, two.lY);
        float z = calculateLinear(one.sec, two.sec, TTT, one.lZ, two.lZ);
        Point ret = new Point(x, y, z, TTT);
        TTT += TIME;
        //}
        return ret;
    }

    private float calculateLinear(float t1, float t2, float t, float y1, float y2) {
        float y = y1 + (t - t1) * ((y2 - y1) / (t2 - t1));
        if(t2==t1){
            y=(y1+y2)/2;
        }
        return y;
    }

    public String toJson(){
        String s = "{ \"Gesture\":{ \"name\":" + "\"" + m_name + "\"" + ", \"TIME\":" + TIME + "," + "\"gesttime\":" + m_gesttime + ",\"points\":[";
        ArrayList<String> points = new ArrayList<>();
        for (int i = 0; i < ps.size(); i++) {
            Point p = ps.get(i);
            String so = "{" + "\"x\":" + p.lX + ",\"y\":" + p.lY + ",\"z\":" + p.lZ + ",\"t\":" + p.sec + "}";
            points.add(so);

        }
        String sp = "";
        for (int i = 0; i < points.size(); i++) {
            sp += points.get(i);
            if (i != points.size() - 1) {
                sp += ",";
            }
        }
        s += sp;
        s += "]}}";
        // s.replace('\"',' ');
        Log.d("json", s);
        return s;

    }

    @Override
    public String toString() {

        String s = "{ \"Gesture\":{ \"name\":" + "\"" + m_name + "\"" + ", \"TIME\":" + TIME + "," + "\"gesttime\":" + m_gesttime + ",\"points\":[";
        ArrayList<String> points = new ArrayList<>();
        for (int i = 0; i < ps.size(); i++) {
            Point p = ps.get(i);
            String so = "{" + "\"x\":" + p.lX + ",\"y\":" + p.lY + ",\"z\":" + p.lZ + ",\"t\":" + p.sec + "}";
            points.add(so);

        }
        String sp = "";
        for (int i = 0; i < points.size(); i++) {
            sp += points.get(i);
            if (i != points.size() - 1) {
                sp += ",";
            }
        }
        s += sp;
        s += "]}}";
        s.replace('\"',' ');
        Log.d("json", s);
        return s;

    }
}

