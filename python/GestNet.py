#! C:\Program Files (x86)\Microsoft Visual Studio\Shared\Anaconda3_64\python.exe
# coding: utf-8

# In[34]:


import pandas as pd


# In[35]:


nClasses=2


# In[36]:


f=pd.read_json('http://localhost/test/index.php?action=get_gestures')


# In[37]:


import numpy as np
import sklearn as sk


# In[38]:


import keras


# In[40]:


x=[]
y=[]
for gest_type in f['Gestures']:
    for gesture in gest_type:
        
        #print(gesture["Gesture"]["name"])
        
        points = gesture["Gesture"]["points"];
        to_ar=[]
        if gesture["Gesture"]["name"]!="Norm":
            for point in points:
                send=[]
                send.append(point["x"])
                send.append(point["y"])
                send.append(point["z"])
                to_ar.append(send)
        #print(len(to_ar))
            x.append(to_ar)
        
        #if gesture["Gesture"]["name"]=="Norm":
            #y.append(0)
        if gesture["Gesture"]["name"]=="LineRight":
            y.append(0)
        if gesture["Gesture"]["name"]=="Round":
            y.append(1)
            


# In[41]:


igrek=[]
for l in y:
    #if l==0:
        #igrek.append([1,0,0])
    if l==0:
        igrek.append([1,0])
    if l==1:
        igrek.append([0,1])
    


# In[44]:


x_i=np.array(x)
y_i=np.array(igrek)


# In[46]:


from sklearn.model_selection import train_test_split


# In[47]:


data_train, data_test, labels_train, labels_test = train_test_split(x_i, y_i, test_size=0.25, random_state=42) 


# In[49]:


from keras.models import Sequential
from keras.layers import Dense, Conv1D, MaxPooling1D, Dropout, Flatten
 
def createModel():
    model = Sequential()
    model.add(Conv1D(32, 3, padding='same', activation='relu', input_shape=(399,3)))
    model.add(Conv1D(32, 3, activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Dropout(0.25))
 
    model.add(Conv1D(64, 3, padding='same', activation='relu'))
    model.add(Conv1D(64, 3, activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Dropout(0.25))
 
    model.add(Conv1D(64, 3, padding='same', activation='relu'))
    model.add(Conv1D(64, 3, activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Dropout(0.25))
 
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2, activation='softmax'))
     
    return model


# In[50]:


model1 = createModel()
batch_size = 1
epochs = 40
model1.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
 
history = model1.fit(data_train, labels_train, batch_size=batch_size, epochs=epochs, verbose=1, 
                   validation_data=(data_test, labels_test))
 
#model1.evaluate(test_data, test_labels_one_hot)


# In[51]:


model_json = model1.to_json()
# Записываем модель в файл
json_file = open("modelauto.json", "w")
json_file.write(model_json)
json_file.close()


# In[54]:


model1.save("C:\\Users\\user\\source\\mmodelauto.h5")

import argparse
parser = argparse.ArgumentParser(description='set input arguments')
parser.add_argument('-input_fld', action="store", 
                    dest='input_fld', type=str, default='.')
parser.add_argument('-output_fld', action="store", 
                    dest='output_fld', type=str, default='')
parser.add_argument('-input_model_file', action="store", 
                    dest='input_model_file', type=str, default='model.h5')
parser.add_argument('-output_model_file', action="store", 
                    dest='output_model_file', type=str, default='')
parser.add_argument('-output_graphdef_file', action="store", 
                    dest='output_graphdef_file', type=str, default='model.ascii')
parser.add_argument('-num_outputs', action="store", 
                    dest='num_outputs', type=int, default=1)
parser.add_argument('-graph_def', action="store", 
                    dest='graph_def', type=bool, default=False)
parser.add_argument('-output_node_prefix', action="store", 
                    dest='output_node_prefix', type=str, default='output_node')
parser.add_argument('-quantize', action="store", 
                    dest='quantize', type=bool, default=False)
parser.add_argument('-theano_backend', action="store", 
                    dest='theano_backend', type=bool, default=False)
parser.add_argument('-f')
args = parser.parse_args()
parser.print_help()
print('input args: ', args)

if args.theano_backend is True and args.quantize is True:
    raise ValueError("Quantize feature does not work with theano backend.")


# initialize

# In[2]:


from keras.models import load_model
import tensorflow as tf
from pathlib import Path
from keras import backend as K

output_fld =  args.input_fld if args.output_fld == '' else args.output_fld
if args.output_model_file == '':
    args.output_model_file = str(Path(args.input_model_file).name) + '.pb'
Path(output_fld).mkdir(parents=True, exist_ok=True)    
weight_file_path = str(Path(args.input_fld) / args.input_model_file)


# Load keras model and rename output

# In[3]:


weight_file_path="C:\\Users\\user\\source\\mmodelauto.h5"
K.set_learning_phase(0)
if args.theano_backend:
    K.set_image_data_format('channels_first')
else:
    K.set_image_data_format('channels_last')

try:
    net_model = load_model(weight_file_path)
except ValueError as err:
    print('''Input file specified ({}) only holds the weights, and not the model defenition.
    Save the model using mode.save(filename.h5) which will contain the network architecture
    as well as its weights. 
    If the model is saved using model.save_weights(filename.h5), the model architecture is 
    expected to be saved separately in a json format and loaded prior to loading the weights.
    Check the keras documentation for more details (https://keras.io/getting-started/faq/)'''
          .format(weight_file_path))
    raise err
num_output = args.num_outputs
pred = [None]*num_output
pred_node_names = [None]*num_output
for i in range(num_output):
    pred_node_names[i] = args.output_node_prefix+str(i)
    pred[i] = tf.identity(net_model.outputs[i], name=pred_node_names[i])
print('output nodes names are: ', pred_node_names)


# [optional] write graph definition in ascii

# In[4]:


sess = K.get_session()

if args.graph_def:
    f = args.output_graphdef_file 
    tf.train.write_graph(sess.graph.as_graph_def(), output_fld, f, as_text=True)
    print('saved the graph definition in ascii format at: ', str(Path(output_fld) / f))


# convert variables to constants and save

# In[5]:


from tensorflow.python.framework import graph_util
from tensorflow.python.framework import graph_io
from tensorflow.tools.graph_transforms import TransformGraph
if args.quantize:
    transforms = ["quantize_weights", "quantize_nodes"]
    transformed_graph_def = TransformGraph(sess.graph.as_graph_def(), [], pred_node_names, transforms)
    constant_graph = graph_util.convert_variables_to_constants(sess, transformed_graph_def, pred_node_names)
else:
    constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph.as_graph_def(), pred_node_names)    
graph_io.write_graph(constant_graph, output_fld, args.output_model_file, as_text=False)
print('saved the freezed graph (ready for inference) at: ', str(Path(output_fld) / args.output_model_file))

import tensorflow as tf 


# In[7]:


g = tf.GraphDef()


# In[8]:


g.ParseFromString(open('C:\\Users\\user\\source\\model.h5.pb', 'rb').read())


# In[9]:


print([n for n in g.node if n.name.find('input') != -1])

