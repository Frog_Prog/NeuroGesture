<?php


// делаем коннект к БД
$conn = null;
try{
    $connect_db = "mysql:host=localhost;dbname=test";
    $db_user = "root";
    $db_pass = "";

    $conn = new PDO($connect_db, $db_user, $db_pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e){
    print_r('Error connect to DB: '. $e->getMessage());
}   

function clearPost ($s)
{
    $s = str_replace('\\','',$s);
    return str_replace("NaN","0.0",$s);
}

// если при вызове скрипта был POST запрос (то есть планируется изменение данных в БД)
if($_POST){
    if($_POST['action'] == 'insert_gesture'){
        try {
            $post_data = $_POST['gesture'];
            $post_data = clearPost($post_data);
            $arr = json_decode($post_data, true);
            
            $name = $arr['Gesture']['name'];

            if($name == null){
                $name = 'error';
            }
            
            $sql = "INSERT INTO `rawdata`( `name`, `data`) VALUES (?,?)";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array($name,$post_data));      
    
        } catch (PDOException $e) {
            die ("Error!: " . $e->getMessage() . "<br/>");
        }
    }
    

}

if($_GET){
    // возвращает список карточек контроллера LM 
    if($_GET['action'] == 'get_gestures'){
        $comm = "select  data from rawdata";
        try{
            $stmt = $conn->prepare( $comm);
            $data = $conn->query($comm)->fetchAll(PDO::FETCH_ASSOC);

            $gestures = array();
            $rows = $stmt;

            foreach ($data as $row) {
                $gesture = json_decode($row['data'], true);
                $name = $gesture["Gesture"]["name"];

                if(isset($gestures[$name])){
                    $gestures[$name][] = $gesture;
                } else {
                    $gestures[$name] = array($gesture);
                }
            }

            echo json_encode(array("Gestures" => $gestures));
        } 
        catch (PDOException $e){
            die('Error'. $e->getMessage());
        }    

        exit;
    }
}

?>                        